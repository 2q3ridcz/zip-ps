# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0]
### Added
- Import-ZipCsv

## [0.0.1]
### Added
- Get-ZipContent
- Test-ZipEntry

[Unreleased]: https://gitlab.com/2q3ridcz/zip-ps/-/compare/v0.1.0...main
[0.1.0]: https://gitlab.com/2q3ridcz/zip-ps/-/tree/v0.1.0
[0.0.1]: https://gitlab.com/2q3ridcz/zip-ps/-/tree/v0.0.1

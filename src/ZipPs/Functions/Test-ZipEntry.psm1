<#
.Synopsis
    Determines whether the entry exists in the zip file.
.DESCRIPTION
    Determines whether the entry exists in the zip file.
.EXAMPLE
    Test-ZipEntry -Path "C:\0731.zip" -EntryName "folder/sample.txt"
#>
function Test-ZipEntry {
    [CmdletBinding()]
    [OutputType([bool])]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSReviewUnusedParameter', '')]
    param (
        [Parameter(Mandatory=$True)]
        [String]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [String]
        $EntryName
    )

    begin { Add-Type -AssemblyName "System.IO.Compression.FileSystem" }

    process {
        try {
            $Zip = [System.IO.Compression.ZipFile]::OpenRead($Path)

            $Null -Ne $Zip.GetEntry($EntryName)
        } Finally {
            If ($Zip) {
                $Zip.Dispose()
                $Zip = $Null
            }
        }
    }

    end {}
}

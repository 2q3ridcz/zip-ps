﻿function Import-ZipCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $EntryName
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Encoding
    )
    begin {}
    process {
        $FuncParam = @{
            "Path" = $Path
            "EntryName" = $EntryName
        }
        If ($PSBoundParameters.ContainsKey("Encoding")) {
            $FuncParam["Encoding"] = $Encoding
        }

        Get-ZipCOntent @FuncParam | ConvertFrom-Csv
    }
    end {}
}
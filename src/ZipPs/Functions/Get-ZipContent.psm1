<#
.Synopsis
    Reads a text file in a zip file and returns a string.
.DESCRIPTION
    Reads a text file in a zip file and returns a string.
.EXAMPLE
    Get-ZipContent -Path "C:\0731.zip" -EntryName "folder/sample.txt"
#>
function Get-ZipContent {
    [CmdletBinding()]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSReviewUnusedParameter', '')]
    param (
        [Parameter(Mandatory=$True)]
        [String]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [String]
        $EntryName
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $Encoding = "utf8"
    )

    begin { Add-Type -AssemblyName "System.IO.Compression.FileSystem" }

    process {
        try {
            $Zip = [System.IO.Compression.ZipFile]::OpenRead($Path)

            $SelectedFile =
            $Zip.Entries |
            ?{ $_.FullName -Like $EntryName } |
            Select-Object -First 1

            $Reader = [System.IO.StreamReader]::new(
                $SelectedFile.Open(),
                [System.Text.Encoding]::$Encoding
            )
            $Reader.ReadToEnd() | Write-Output
        } Finally {
            If ($Reader) {
                $Reader.Dispose()
                $Reader = $Null
            }
            If ($Zip) {
                $Zip.Dispose()
                $Zip = $Null
            }
        }
    }

    end {}
}

function Assert-Object {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [AllowEmptyCollection()]
        [object[]]
        $Result
        ,
        [Parameter(Mandatory=$True)]
        [AllowEmptyCollection()]
        [object[]]
        $Expect
        ,
        [Parameter(Mandatory=$False)]
        [switch]
        $IgnoreType
    )
    begin {}
    process {
        $Result.Length | Should Be $Expect.Length

        for ($i = 0; $i -lt $Result.Length; $i++) {
            $ResultI = $Result[$i]
            $ExpectI = $Expect[$i]

            $MemberList = @($ResultI | Get-Member -MemberType Properties | %{ $_.Name })
            $ExpectMemberList = @($ExpectI | Get-Member -MemberType Properties | %{ $_.Name })
            $MemberList.Length | Should Be $ExpectMemberList.Length

            for ($j = 0; $j -lt $MemberList.Length; $j++) {
                $ResultMemberI = $MemberList[$j]
                $ExpectMemberI = $ExpectMemberList[$j]
                $ResultMemberI | Should Be $ExpectMemberI
                IF (-Not $IgnoreType) {
                    $ResultI.($ResultMemberI).GetType().Name | Should Be $ExpectI.($ExpectMemberI).GetType().Name
                }
                $ResultI.($ResultMemberI) | Should Be $ExpectI.($ExpectMemberI)
            }
        }
    }
    end {}
}

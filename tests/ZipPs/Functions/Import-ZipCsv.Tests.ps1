﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Import-ZipCsv" {
    Context "Unit test" {
        BeforeEach {
            $FileBaseName = [Int64](
                Get-ChildItem -Path "$TestDrive\*.txt" | Sort-Object -Descending | Select-Object -First 1 |%{ $_.BaseName }
            ) + 1
            $TextFilePath = "$TestDrive\$FileBaseName.txt"
            $ZipFilePath = "$TestDrive\$FileBaseName.zip"
        }

        $testCases = @(
            @{ Encoding = "unicode" }
            @{ Encoding = "bigendianunicode" }
            @{ Encoding = "utf8" }
            @{ Encoding = "utf7" }
            @{ Encoding = "utf32" }
            ### Comment out because this succeeds in local PC but fails on GitLab CI.
            # @{ Encoding = "default" }
        )
        It "Reads <Encoding> encoded file" -TestCases $testCases {
            param ($Encoding)

            $Expect = @(
                New-Object -TypeName psobject -Property @{"string"="abcde";"int"=123}
                New-Object -TypeName psobject -Property @{"string"="fghij";"int"=456}
                New-Object -TypeName psobject -Property @{"string"="klmno";"int"=789}
            )

            $Expect | Export-Csv -Path $TextFilePath -Encoding $Encoding -NoTypeInformation
            Compress-Archive -Path $TextFilePath -DestinationPath $ZipFilePath

            $Result =
            Import-ZipCsv -Path $ZipFilePath -EntryName ($TextFilePath | Split-Path -Leaf) -Encoding $Encoding

            Assert-Object -Result $Result -Expect $Expect -IgnoreType
        }
    }
}

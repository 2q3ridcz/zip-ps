﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "Test-ZipEntry" {
    Context "Unit test" {
        It "Throws when Path is null" {
            {Test-ZipEntry -Path $Null -EntryName "Any"} | Should Throw
        }

        It "Throws when Path is " {
            {Test-ZipEntry -Path "" -EntryName "Any"} | Should Throw
        }

        It "Throws when zip file does not exist" {
            {Test-ZipEntry -Path "$TestDrive\NotExist.zip" -EntryName "Any"} | Should Throw
        }

        It "Throws when EntryName is null" {
            {Test-ZipEntry -Path $Null -EntryName $Null} | Should Throw
        }

        It "Throws when EntryName is " {
            {Test-ZipEntry -Path $Null -EntryName ""} | Should Throw
        }

        $testCases = @(
            @{ "TestCase" = "child file of root"; "EntryName" = "test.txt"; "Expect" = $True }
            @{ "TestCase" = "child file of non root folder"; "EntryName" = "Folder-1\test2.txt"; "Expect" = $True }
            @{ "TestCase" = "folder with children"; "EntryName" = "Folder-1\"; "Expect" = $False }
            @{ "TestCase" = "folder without children"; "EntryName" = "Folder-1\Folder-2\"; "Expect" = $True }
            @{ "TestCase" = "invalid child file of root"; "EntryName" = "test2.txt"; "Expect" = $False }
            @{ "TestCase" = "invalid child file of non root folder"; "EntryName" = "Folder-1\test2a.txt"; "Expect" = $False }
            @{ "TestCase" = "invalid child folder of root"; "EntryName" = "Folder-2\"; "Expect" = $False }
            @{ "TestCase" = "invalid child folder of non root folder"; "EntryName" = "Folder-1\Folder-2a\"; "Expect" = $False }
        )
        It "Returns <Expect> when entry name indicates <TestCase>" -TestCases $testCases {
            param ($EntryName, $Expect)

            Get-ChildItem -Path $TestDrive | Remove-Item -Recurse

            $Root = $TestDrive.FullName
            $ZipFilePath = $Root | Join-Path -ChildPath "abcd-efgh.zip"

            # - (Root)
            #     - test.txt
            #     - Folder-1
            #         - test2.txt
            #         - Folder-2

            $TestFilePath = $Root | Join-Path -ChildPath "test.txt"
            "TestData" | Out-File -FilePath $TestFilePath

            $TestFolder = $Root | Join-Path -ChildPath "Folder-1"
            New-Item -Path $TestFolder -ItemType Directory | Out-Null

            $Test2FilePath = $Root | Join-Path -ChildPath "Folder-1/test2.txt"
            "TestData2" | Out-File -FilePath $TeSt2FilePath

            $Test2FolderPath = $Root | Join-Path -ChildPath "Folder-1/Folder-2"
            New-Item -Path $Test2FolderPath -ItemType Directory | Out-Null

            $CompressArgs = @{
                "Path" = @(
                    $TestFolder
                    $TestFilePath
                )
                "DestinationPath" = $ZipFilePath
            }
            Compress-Archive @CompressArgs

            $Result = Test-ZipEntry -Path $ZipFilePath -EntryName $EntryName
            $Result | Should Be $Expect
        }
    }
}

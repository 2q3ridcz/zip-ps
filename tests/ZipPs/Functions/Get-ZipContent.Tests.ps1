﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "Get-ZipContent" {
    Context "Unit test" {
        BeforeEach {
            $FileBaseName = [Int64](
                Get-ChildItem -Path "$TestDrive\*.txt" | Sort-Object -Descending | Select-Object -First 1 |%{ $_.BaseName }
            ) + 1
            $TextFilePath = "$TestDrive\$FileBaseName.txt"
            $ZipFilePath = "$TestDrive\$FileBaseName.zip"
        }

        It "Reads text file in a zip file without decompressing whole zip file" {
            $Expect =
            "abcde`r`nfghij" |
            %{ $_ -Split ("`r`n") } |
            %{ $_ -Split ("`n") } |
            ?{ $_ -Ne "" }

            $Expect | Out-File -FilePath $TextFilePath
            Compress-Archive -Path $TextFilePath -DestinationPath $ZipFilePath

            $Result =
            Get-ZipContent -Path $ZipFilePath -EntryName ($TextFilePath | Split-Path -Leaf) |
            %{ $_ -Split ("`r`n") } |
            %{ $_ -Split ("`n") } |
            ?{ $_ -Ne "" }

            $Result.Length | Should Be $Expect.Length
            foreach ($i in (0..($Expect.Length - 1))) {
                $Result[$i] | Should Be $Expect[$i]
            }
        }

        $testCases = @(
            @{ Encoding = "unicode" }
            @{ Encoding = "bigendianunicode" }
            @{ Encoding = "utf8" }
            @{ Encoding = "utf7" }
            @{ Encoding = "utf32" }
            ### Comment out because this succeeds in local PC but fails on GitLab CI.
            # @{ Encoding = "default" }
        )
        It "Reads <Encoding> encode file" -TestCases $testCases {
            param ($Encoding)

            $Expect =
            "あいうえお`r`nかきくけこ" |
            %{ $_ -Split ("`r`n") } |
            %{ $_ -Split ("`n") } |
            ?{ $_ -Ne "" }

            $Expect | Out-File -FilePath $TextFilePath -Encoding $Encoding
            Compress-Archive -Path $TextFilePath -DestinationPath $ZipFilePath

            $Result =
            Get-ZipContent -Path $ZipFilePath -EntryName ($TextFilePath | Split-Path -Leaf) -Encoding $Encoding |
            %{ $_ -Split ("`r`n") } |
            %{ $_ -Split ("`n") } |
            ?{ $_ -Ne "" }

            $Result.Length | Should Be $Expect.Length
            foreach ($i in (0..($Expect.Length - 1))) {
                $Result[$i] | Should Be $Expect[$i]
            }
        }
    }
}

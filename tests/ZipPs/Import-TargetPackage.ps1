$here = Split-Path -Parent $MyInvocation.MyCommand.Path

$TestsFolder = Get-Item -Path "$here\..\"
foreach ($PackageName in @("TestHelper")) {
    $PackagePath = $TestsFolder.FullName | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
    Import-Module -Name $PackagePath -Force
}

$ProjectFolder = Get-Item -Path "$here\..\..\"
foreach ($PackageName in @("ZipPs")) {
    $PackagePath = $ProjectFolder.FullName | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
    Import-Module -Name $PackagePath -Force
}
